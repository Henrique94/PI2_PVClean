#include <SPI.h>
#include "Enrf24.h"
#include "nRF24L01.h"
#include <string.h>
#include <Servo.h> 

//Pino ligado ao servo motor
#define pino_servo P2_3

Servo servo;
boolean DEBUG_MODE = true;

int cima = 0;
int baixo = 180;
Enrf24 radio(P2_0, P2_1, P2_2);  // P2.0=CE, P2.1=CSN, P2.2=IRQ
const uint8_t txaddr[] = { 0xE7, 0xD3, 0xF0, 0x35, 0xFF }; //Este e o endereco para onde estou transmitindo
const uint8_t rxaddr[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x02 }; //Este e o endereco de recepcao

// Pino ligado ao sensor de nivel de liquido
#define pinosensor P2_4

//Pino ligado a bomba d'agua
#define BOMBA P1_4

//Pino ligado ao sensor de Chuva
#define chuva_01 P2_5 
#define chuva_02 P1_1 

const int max_payload_size = 16; // Default e 32
unsigned char receive_payload[max_payload_size + 1];
const unsigned int delay_no_debug = 100; // Tempo de delay no caso de nao estar no modo debug
int recebidos[1];
int teste = 1;
void setup()
{
   servo.attach(pino_servo); 
   servo.write(0);                //Garante que o servo sempre inicie na posicao 0
   pinMode(pinosensor, INPUT);  
   pinMode(BOMBA, OUTPUT);
   pinMode(BOMBA, HIGH);
   digitalWrite(BOMBA, LOW);

    if(DEBUG_MODE)
    {
      Serial.begin(9600);
      Serial.print("Aguardando... ");
    }

    SPI.begin();
    SPI.setDataMode(SPI_MODE0);
    SPI.setBitOrder(MSBFIRST);

    radio.begin( 1000000, 76 );  // Padrao 1Mbps, canal 0
    radio.setCRC(1, 1);

    radio.setTXaddress((void*)txaddr);
    radio.setRXaddress((void*)rxaddr);
    
    radio.enableRX();
}

void leitura_sensor_chuva_1(){
  int estado_01 = digitalRead(chuva_01);
  Serial.print("Estado sensor 01 : ");
  Serial.println(estado_01);
  radio.write(estado_01);
  delay(300);
}

void leitura_sensor_chuva_2(){
  int estado_02 = digitalRead(chuva_02);
  Serial.print("Estado sensor 02 : ");
  Serial.println(estado_02);
  radio.write(estado_02);
  delay(300);
}

void leitura_sensor_nivel(){
  int estado = digitalRead(pinosensor); 
  Serial.print("Sensor de Nivel: ");
  Serial.println(estado);
  radio.write(estado);
  delay(300); 
}

void loop()
{
  while(1){
    // Se houver dados disponiveis
    if (radio.available())
    {
        // Obter as cargas úteis até que tenhamos conseguido tudo
        uint8_t len;
        
        while (radio.available())
        {
            // Buscar a carga, e ver se este foi o último.
            len = radio.read(recebidos);
            
            // Imprime o o que foi recebido
            if(DEBUG_MODE)
            {
                Serial.print("Informacao recebida:");
                Serial.print(recebidos[0]);
            }
            
            // Coloca um 0 no final para facilitar a impressao.
            receive_payload[len] = 0;
            
            if(!DEBUG_MODE)
            {
                delay(delay_no_debug);
            }
        }
        
        // Para a escuta para poder transmitir.
        radio.disableRX();

        int comando = recebidos[0];
        if(comando == 0) 
        {
            leitura_sensor_chuva_1();
        }
        else if(comando == 1) 
        {
            leitura_sensor_chuva_2();
        }

        else if(comando == 2) 
        {
            leitura_sensor_nivel();
        }
        else if(comando == 3) 
        {
            digitalWrite(BOMBA, HIGH);
            Serial.print("\nbomba ligada");
            radio.write("1");
        }
        else if(comando == 4)
        {
            digitalWrite(BOMBA, LOW);
            Serial.print("bomba desligada");
            radio.write("1");
        }
        else if(comando == 5) // Calha para baixo
        {
            servo.write(baixo);
            Serial.print("Calha para baixo");
            radio.write("1");
            delay(300);
        }
	
	else if(comando == 6) // Calha para cima
        {
            servo.write(cima);
            Serial.print("Calha para cima");
            radio.write("1");
            delay(300);
        }
	
        else
        {
            radio.write("Comando invalido");
        }
        
        radio.flush();
        
        if(DEBUG_MODE)
        {
            Serial.println("\nResposta enviada.");
        }
        
        // Voltando a escutar para poder capturar novos pacotes.
        radio.enableRX();
    }
  }
}
