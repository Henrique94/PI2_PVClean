#include <SPI.h>
#include <printf.h>
#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>
#include <Servo.h>
#include <Thread.h>
#include <ThreadController.h>
#include <Stepper.h>

// ----- Configuraço do rdio-----------------
RF24 radio(9,10);

const uint64_t linkReading = 0xE7D3F035FF;            // Este e o endereco de recepcao
const uint64_t linkWriting = 0xDEADBEEF02;            // Este e o endereco para onde estou transmitindo

const int max_payload_size = 16;                      // Tamanho máximo da mensagem (defaul é 32)
unsigned char receive_payload[max_payload_size + 1];  // +1 to allow room for a terminating NULL char
unsigned long max_time = 500;                         // Tempo para timeout
const int max_amount_timeout = 10;                    // Quantidade máxima de timeouts permitidos
int amount_timeout = 0;   // Contador de timeouts
unsigned int mensagem_radio;
// Armazena caractere digitado na serial
char valor_2[1];

// -----------Configuraço das Threds-----------------

ThreadController cpu;

Thread threadSerial;
Thread threadMotor;
Thread threadTemperatura;
Thread threadBateria;

Servo rodo;
int angulo_rodo = 45;
char dados_recebidos[256];

float tempC, temp_total, temperatura_maxima;
char temp_media, lixo, tensao_bateria;
int tempPin = 0; // Declara o pin analógico 0
int cooler = 2;
int tempo = 10;

int analogInput = 1;
float vout = 0.0;
float vin = 0.0;
float R1 = 100000.0; // resistance of R1 (100K) -see text!
float R2 = 10000.0; // resistance of R2 (10K) - see text!
int value = 0;
int num_inteiro = 0;

// Conexoes modulo - Arduino
int IN1 = 5;
int IN2 = 6;
int IN3 = 7;
int IN4 = 8;

typedef union {
 float valor_float;
 byte bytes[4];
} bytesFloat;

typedef union {
 long int valor_int;
 byte bytes[4];
} bytesInt;

/*------------------------------------------------------
 *  Funções Auxiliares
 ----------------------------------------------------- */
void le_uart(int num_bytes) {
  int i = 0;
  while(i < num_bytes) {
    if(Serial.available()) {
      dados_recebidos[i] = Serial.read();
      //Serial.print(dados_recebidos[i]);
      i++;
    }
  }
}

void le_inteiro() {
  num_inteiro = 0;
  le_uart(4);
  memcpy(&num_inteiro, dados_recebidos, 4);
}

void envia_float(float dado) {
  bytesFloat envia_dado;
  envia_dado.valor_float = dado;
  Serial.write(envia_dado.bytes,4);
}


void setup()
{
  // Define os pinos como saida
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
 
  pinMode (cooler, OUTPUT);
  pinMode(analogInput, INPUT);
  pinMode(tempPin, INPUT);
  rodo.attach(3);
  
 // Iniciando e configuranco o radio
    Serial.begin(9600);
    radio.begin();
   
   // Opcionalmente, incrementa o tempo entre as tentativas
    radio.setRetries(15, 15);
   
    radio.setPALevel(RF24_PA_MAX);
    radio.enableDynamicPayloads();
    radio.setCRCLength(RF24_CRC_16);

    radio.openWritingPipe(linkWriting);
    radio.openReadingPipe(1, linkReading); 
    
/*------------------------------------------------------
 *  Configuração das Thread
 ----------------------------------------------------- */
threadSerial.setInterval(100);
threadSerial.onRun(ler_serial);

threadMotor.setInterval(500);
threadMotor.onRun(aciona_motor);

threadTemperatura.setInterval(300);
threadTemperatura.onRun(ler_temperatura);

threadBateria.setInterval(400);
threadBateria.onRun(ler_bateria);

cpu.add(&threadSerial);
cpu.add(&threadTemperatura);
cpu.add(&threadBateria);
    
}

/*------------------------------------------------------
 *  Configuração de Variáveis Globais
 ----------------------------------------------------- */
#define CMD_ACIONA_MOTOR_PASSO    0xA1
#define CMD_ACIONA_CALHA          0XA2
#define CMD_ACIONA_BOMBA          0XA3

#define CMD_LER_TEMPERATURA       0XB1
#define CMD_LER_BATERIA           0XB2
#define CMD_LER_SENSORES          0XB3
#define CMD_ERROR 0xE1

unsigned char dado_recebe;
unsigned char valor;
unsigned int contador_tempo = 0;

//Variáveis da função Aciona_motor_passo:
int anda_passos;
unsigned char direcao;
unsigned char mover, bomba, sensor_nivel, sensor_chuva_1, sensor_chuva_2;
unsigned char retorno[5] = {1,2,3,4,5};
unsigned char sensores[4];
int i;



/*------------------------------------------------------
 *  Funções das Threads
 ----------------------------------------------------- */
 
 unsigned char radio_comunica()
{
  
    // Inciando o módulo de RF.
    radio.startListening();
    
    // Tentando enquanto não estoura a quantidade máxima de timeouts
    while (amount_timeout < max_amount_timeout)
    {            
        // Primeiro, para a escuta para poder transmitir.
        radio.stopListening();
        
        //DEUBG
        Serial.print("Estou aki");
        
        // Agora envia. O radio fica "travado" até que a transmissão seja completada.
        radio.write(&mensagem_radio, sizeof(unsigned int));
        
        // Agora volta a escutar...
        radio.startListening();
        
        // Espera a resposta ou timeout.
        unsigned long started_waiting_at = millis();
        bool timeout = false;
        while (!radio.available() && !timeout)
        {
            if (millis() - started_waiting_at > max_time )
            {
                timeout = true;
                amount_timeout++;
            }   
        }
        
        // Retorna o resultado
        if (!timeout)
        {
            // Pega a resposta e compara.
            uint8_t len = radio.getDynamicPayloadSize();
            radio.read(receive_payload, len);
            
            // Coloca um 0 no final para facilitar a impressão em tela.
            receive_payload[len] = 0;
            
            // Imprime a resposta
            Serial.print(receive_payload[0]);
            
             //int retorno = (int)receive_payload;
   	     //printf("Retorno do radio: %d", retorno);
    	     return receive_payload[0];
 		
        }
        
        // Espera um pouco para tentar novamente.
        delay(100);
    }
    
    amount_timeout = 0; 
    Serial.println("\nO dispositivo remoto não respondeu\n");
    return 0;
}
void ler_serial(){
  
  unsigned char retorno_radio;
  
     if(Serial.available()) {
     dado_recebe = Serial.read();
     Serial.println(dado_recebe);
    switch (dado_recebe) {   
// ------------------------------------------------------    
// COMANDOS DE LEITURA DE SENSORES
// ------------------------------------------------------
      case CMD_ACIONA_MOTOR_PASSO:
            le_inteiro();
            direcao = Serial.read();    
            Serial.println(num_inteiro);
            Serial.println(direcao);  
            threadMotor.run();
             //Serial.write(1);
        break;

     case CMD_ACIONA_CALHA:
            mover = Serial.read();
            Serial.println(mover);
          
            if (mover == 1){           //move calha para baixo
             mensagem_radio = 5;
             retorno_radio = radio_comunica();      //manda 1 quando recebeu a mensagem de retorno e 0 quando nao
             Serial.println(retorno_radio);
             Serial.write(retorno_radio);   
           
            }else if (mover ==0){   //move calha para cima
             mensagem_radio = 6;
             retorno_radio = radio_comunica();      //manda 1 quando recebeu a mensagem de retorno e 0 quando nao
             Serial.println(retorno_radio);
             Serial.write(retorno_radio); 
            }
            break;

      case CMD_ACIONA_BOMBA:
            bomba = Serial.read();
            Serial.println(bomba);
            if(bomba == 1){           //ativa bomba
              mensagem_radio = 3;
              retorno_radio = radio_comunica();
              Serial.println(retorno_radio);
              Serial.write(retorno_radio); 
            }else if (bomba == 0){   //move calha para cima
             mensagem_radio = 4;
             retorno_radio = radio_comunica();
             Serial.println(retorno_radio);
             Serial.write(retorno_radio); 
            }
            break;

      case CMD_LER_BATERIA:
            ler_bateria;
            Serial.write(tensao_bateria);
            break;
            
      case CMD_LER_TEMPERATURA:
        ler_temperatura();
        Serial.write(temp_media);
        break;
       
      case  CMD_LER_SENSORES:
        ler_temperatura();
        ler_bateria();
        mensagem_radio = 0;
        sensor_chuva_1 = radio_comunica();
        sensores[2] = sensor_chuva_1;
        mensagem_radio = 1;
        sensor_chuva_2 = radio_comunica();
        sensores[3] = sensor_chuva_2;
        mensagem_radio = 2;
        sensor_nivel = radio_comunica();
        sensores[4] = sensor_nivel;
        for(i = 0; i <= 4; i++) {
          Serial.println("\nSensor: ");
          Serial.println(sensores[i]);
          //Serial.write(sensores[i]);
        }
      break;  
        default:
        //Serial1.write(CMD_ERROR);   
        break;
      
    } // End Switch
    
  } // End IF INICIAL
  delay(10);
}

void aciona_motor(){
    if(direcao == 1){
        for(int i = 0; i<= num_inteiro; i++){
        
               // Passo 1
              digitalWrite(IN1, 1);
              digitalWrite(IN2, 0);
              digitalWrite(IN3, 0);
              digitalWrite(IN4, 1);
              delay(tempo);  
            
              // Passo 2
              digitalWrite(IN1, 0);
              digitalWrite(IN2, 1);
              digitalWrite(IN3, 0);
              digitalWrite(IN4, 1);
              delay(tempo);
            
              // Passo 3
              digitalWrite(IN1, 0);
              digitalWrite(IN2, 1);
              digitalWrite(IN3, 1);
              digitalWrite(IN4, 0);
              delay(tempo);
              // Passo 4
              digitalWrite(IN1, 1);
              digitalWrite(IN2, 0);
              digitalWrite(IN3, 1);
              digitalWrite(IN4, 0);
              delay(tempo);
        }
     
        }else if(direcao == 0){
        rodo.write(angulo_rodo);
        delay(1000); 
         for(int i = 0; i<= num_inteiro; i++){
               // Passo 1
              digitalWrite(IN1, 1);
              digitalWrite(IN2, 0);
              digitalWrite(IN3, 1);
              digitalWrite(IN4, 0);
              delay(tempo);  
            
              // Passo 2
              digitalWrite(IN1, 0);
              digitalWrite(IN2, 1);
              digitalWrite(IN3, 1);
              digitalWrite(IN4, 0);
              delay(tempo);
            
              // Passo 3
              digitalWrite(IN1, 0);
              digitalWrite(IN2, 1);
              digitalWrite(IN3, 0);
              digitalWrite(IN4, 1);
              delay(tempo);
              // Passo 4
              digitalWrite(IN1, 1);
              digitalWrite(IN2, 0);
              digitalWrite(IN3, 0);
              digitalWrite(IN4, 1);
              delay(tempo);
        }
        delay(100);
        rodo.write(0);
        
    }
}

void ler_temperatura(){

  for(int i=0; i<=10; i++){
  tempC = analogRead(tempPin); // Le o valor analógico do sensor LM35
  tempC = (5.0 * tempC * 100.0)/1024.0;
  temp_total = tempC + temp_total;
  }
  temp_media = temp_total/10;
  if (temp_media > temperatura_maxima){
  digitalWrite(cooler, 1);
  }else{
  digitalWrite(cooler, 0);
  }
  sensores[0] = temp_media;
}

void ler_bateria(){
  float total = 0;
  
    for(int i = 0; i<= 10; i++){
       value = analogRead(analogInput);   // Faz a leitura da tensão da bateria
       total = total + value;
       delay(100);
    }
    total = total/10;
   vout = (total * 5.0) / 1024.0; 
   vin = vout / (R2/(R1+R2)); 
   tensao_bateria = vin;
    sensores[1] = tensao_bateria;
   }

/*------------------------------------------------------
 *  Loop Principal
 ----------------------------------------------------- */

void loop(){
  cpu.run(); 
}


