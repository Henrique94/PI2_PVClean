//Bicliotecas

    #include <stdio.h>
    #include <sys/socket.h>
    #include <arpa/inet.h>	
    #include <stdlib.h>
    #include <string.h>	
    #include <unistd.h>	
    #include <fcntl.h>
    #include <termios.h>
    #include <pthread.h>
    #include <semaphore.h>
    #include <signal.h>
    #include <sys/signal.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <time.h>
    #include <math.h>
    #include <string.h>

void catch_signal(int sig);   //Tratamento de sinal 
void cleanup(void);	      //Tratamento de sinal
void verifica_condicoes_iniciais();

//---------------------------------------------------------Variáveis Globais------------------------------------------------------------------------------
 
 int porta = 8006;
 int uart0_filestream;
 float placa = 57.3;    		       	//distancia horizontal de uma placa
 float diametro_engrenagem = 5.00;		//distancia do diametro da engrenagem do motor
 int numero_passos = 200;			// 200 passos por revolução
 float pi = 3.14;				
 float passo;
 float velocidade = 10.00;
 int posicao_inicial;
 int posicao_final;
 int anda_passo;
 unsigned char mensagem[12];
 int socket_Servidor, socket_Cliente;
unsigned char tx_buffer_sensores[20];

//-------------------------------------------Mensagens de Log ------------------------------------------------------------------------

//Mensagens de LOG dos sensores
char inicio [] = "Sistema foi iniciado";
char fechar [] = "Sistema foi encerrado";
char dado1 [] = "Temperatura interna do robô: ";
char dado2 [] = "Tensao da bateria:";
char dado3 [] = "Chuva no setor 1";
char dado4 [] = "Nao ha chuva setor 1";
char dado5 [] = "Chuva no setor 2";
char dado6 [] = "Não ha chuva no setor 2";
char dado7 [] = "Reservatorio esta com agua suficiente para realizar limpeza";
char dado8 [] = "Reservatorio não esta com agua sufuciente";

//Mensagem de LOG de erros
char serial [] = "Erro - Porta Serial nao pode ser aberta.";
char calha [] = "Erro na transmissao - UART TX referente ao acionamento da calha";
char leitura_calha [] = "Falha na leitura do retorno do servo motor externo";
char dado_calha [] = "Nenhum dado disponpivel de retorno do servo motor externo";

//--------------------------------------------Arquivo de LOG ----------------------------------------------------------------------------

void log_float(char *palavra, float valor){
	
	unsigned char tx_buffer[256];
	int dia,mes,ano, hora,min,sec;
	
	struct tm *local;
	time_t t;
	t= time(NULL);
	local=localtime(&t);
	
	hora=local->tm_hour;
	min=local->tm_min;
	sec=local->tm_sec;
	dia=local->tm_mday;
	mes=local->tm_mon+1;
	ano=local->tm_year+1900;
	
  FILE *pont_arq; // cria variável ponteiro para o arquivo
 
  //abrindo o arquivo com tipo de abertura w
  pont_arq = fopen("arquivo_log_sensores.txt", "a");
 
  //testando se o arquivo foi realmente criado
  if(pont_arq == NULL)
  {
     printf("Erro na abertura do arquivo!");
  }
 
  //usando fprintf para armazenar a string no arquivo
  fprintf(pont_arq, "%s: " "%f " "horario %d:" "%d:" "%d " "Data %d/""%d/""%d\n", palavra, valor, hora, min, sec, dia, mes, ano);
 
  //usando fclose para fechar o arquivo
  fclose(pont_arq);

}

void arquivo(char *palavra){
	
	unsigned char tx_buffer[256];
	int dia,mes,ano, hora,min,sec;
	
	struct tm *local;
	time_t t;
	t= time(NULL);
	local=localtime(&t);
	
	hora=local->tm_hour;
	min=local->tm_min;
	sec=local->tm_sec;
	dia=local->tm_mday;
	mes=local->tm_mon+1;
	ano=local->tm_year+1900;
	
  FILE *pont_arq; // cria variável ponteiro para o arquivo
 
  //abrindo o arquivo com tipo de abertura w
  pont_arq = fopen("arquivo_log_sensores.txt", "a");
 
  //testando se o arquivo foi realmente criado
  if(pont_arq == NULL)
  {
     printf("Erro na abertura do arquivo!");
  }
 
  //usando fprintf para armazenar a string no arquivo
  fprintf(pont_arq, "%s " "horario %d:" "%d:" "%d " "Data %d/""%d/""%d\n", palavra,hora, min, sec, dia, mes, ano);
 
  //usando fclose para fechar o arquivo
  fclose(pont_arq);

}

void arquivo2(char *palavra){
	
	unsigned char tx_buffer[256];
	int dia,mes,ano, hora,min,sec;
	
	struct tm *local;
	time_t t;
	t= time(NULL);
	local=localtime(&t);
	
	hora=local->tm_hour;
	min=local->tm_min;
	sec=local->tm_sec;
	dia=local->tm_mday;
	mes=local->tm_mon+1;
	ano=local->tm_year+1900;
	
  FILE *pont_arq; // cria variável ponteiro para o arquivo
 
  //abrindo o arquivo com tipo de abertura w
  pont_arq = fopen("arquivo_log_erro.txt", "a");
 
  //testando se o arquivo foi realmente criado
  if(pont_arq == NULL)
  {
     printf("Erro na abertura do arquivo!");
  }
 
  //usando fprintf para armazenar a string no arquivo
  fprintf(pont_arq, "%s " "horario %d:" "%d:" "%d " "Data %d/""%d/""%d\n", palavra,hora, min, sec, dia, mes, ano);
 
  //usando fclose para fechar o arquivo
  fclose(pont_arq);

}

//---------------------------------------------------Configuração da UART------------------------------------------------------------------

void config_uart(){   //Configuração da UART

	uart0_filestream = -1;
	uart0_filestream = open("/dev/ttyACM10", O_RDWR, O_NDELAY, O_NOCTTY);    //Abrir no modo não blocante para Leitura/Escrita
	if (uart0_filestream == -1){
	printf("Erro - Porta Serial nao pode ser aberta. Confirme se nao esta sendo usada por outra aplicacao.\n");}
	arquivo2(serial);

	struct termios options;
	tcgetattr(uart0_filestream, &options);
	options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;  // Set baud rate
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart0_filestream, TCIFLUSH);
	tcsetattr(uart0_filestream, TCSANOW, &options);
}

//---------------------------------------------Limpar a porta serial do Arduino-------------------------------------------------------------------

void limpar_serial(){

	unsigned char p_tx_buffer []= {0xA1, 0, 0, 0};
	config_uart(); //configuração da uart
	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 4);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}
	}
}


//------------------------------------------------------Acionamento dos atuadores -------------------------------------------------------------------

int aciona_calha(int mover){

unsigned char p_tx_buffer[]= {0xA2, mover};        
	config_uart(); //configuração da uart
	int rx_buffer;

	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 2);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
	arquivo2(calha);	
	}
}

//Fecha UART
	close(uart0_filestream);
	return 0;
}

int aciona_bomba(int bomba){

unsigned char p_tx_buffer[]= {163, bomba};        
	config_uart(); //configuração da uart

	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 2);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}
}


//Fecha UART
	close(uart0_filestream);
}

void passa_codigo(){

	unsigned char p_tx_buffer[]= {161};        
	config_uart(); //configuração da uart

	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 1);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}
}
//Fecha UART
	close(uart0_filestream);
}


void aciona_motor_passo(int anda_passos){
      
	unsigned char p_tx_buffer[256];
 
	memcpy(&p_tx_buffer[0], &anda_passos, 4);
	config_uart(); //configuração da uart

	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 4);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}

}
//Fecha UART
	close(uart0_filestream);
}

int passa_parametro(int direcao){
unsigned char p_tx_buffer1[] = {1};
unsigned char p_tx_buffer2[] = {0};

	if(direcao == 1){

	config_uart(); //configuração da uart
	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer1, 1);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos;
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		      }
	}
}
	if(direcao == 0){
	config_uart(); //configuração da uart
	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer2, 1);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos;
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
			
		}
	}
}

//Fecha UART
	close(uart0_filestream);

}
//------------------------------------------Leitura dos Sensores---------------------------------------------------------------------------

int ler_sensores(){

while(1){

	unsigned char p_tx_buffer []= {0xB3};
	config_uart(); //configuração da uart
	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 1);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos;
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}
	}
/*
//RX - Aguarda resposta de retorno 
	float temperatura, bateria;
	int sensor_chuva_1, sensor_chuva_2, sensor_nivel;
	if (uart0_filestream != -1){
	int rx_length = read(uart0_filestream, &temperatura,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length < 0){
	perror("Falha na leitura da temperatura\n");
	return -1;
	}
	else if (rx_length == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nTemperatura: %f \n",temperatura);
		} 
	}



//RX - Aguarda resposta de retorno 
	
	if (uart0_filestream != -1){
	int rx_length2 = read(uart0_filestream, &bateria,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length2 < 0){
	perror("Falha na leitura da bateria\n");
	return -1;
	}
	else if (rx_length2 == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nTensão da Bateria: %f \n",bateria);
		} 
	}


//RX - Aguarda resposta de retorno 
	
	if (uart0_filestream != -1){
	int rx_length3 = read(uart0_filestream, &sensor_chuva_1,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length3 < 0){
	perror("Falha na leitura do sensor de chuva 1\n");
	return -1;
	}
	else if (rx_length3 == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nSensor de chuva 1: %d \n",sensor_chuva_1);
		} 
	}


//RX - Aguarda resposta de retorno 
	
	if (uart0_filestream != -1){
	int rx_length4 = read(uart0_filestream, &sensor_chuva_2,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length4 < 0){
	perror("Falha na leitura do sensor de chuva 2\n");
	return -1;
	}
	else if (rx_length4 == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nSensor de chuva 2: %d \n",sensor_chuva_2);
		} 
	}

//RX - Aguarda resposta de retorno 
	
	if (uart0_filestream != -1){
	int rx_length5 = read(uart0_filestream, &sensor_nivel,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length5 < 0){
	perror("Falha na leitura do sensor de nível\n");
	return -1;
	}
	else if (rx_length5 == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nSensor de nível: %d \n",sensor_nivel);
		} 
	}

	    memcpy(&tx_buffer_sensores[0], &temperatura, 4);
	    memcpy(&tx_buffer_sensores[4], &bateria, 4);
	    memcpy(&tx_buffer_sensores[8], &sensor_chuva_1, 4);
            memcpy(&tx_buffer_sensores[12],&sensor_chuva_2, 4);
	    memcpy(&tx_buffer_sensores[16],&sensor_nivel, 4);


log_float(dado1, temperatura);
log_float(dado2, bateria);
	if(sensor_chuva_1 == 1){
	arquivo(dado3);
	}else{
	arquivo(dado4);
	}

	if(sensor_chuva_2 == 1){
	arquivo(dado5);
	}else{
	arquivo(dado6);
	}

	if(sensor_nivel == 1){
	arquivo(dado7);
	}else{
	arquivo(dado8);
	}
*/
//Fecha UART
	close(uart0_filestream);
	return 0;
}
}

float temperatura(){

	unsigned char p_tx_buffer []= {0xB1};
	config_uart(); //configuração da uart
	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 1);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos;
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}
	

//RX - Aguarda resposta de retorno 
	
	if (uart0_filestream != -1){
	float rx_buffer;
	int rx_length = read(uart0_filestream, &rx_buffer,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length < 0){
	perror("Falha na leitura\n");
	return -1;
	}
	else if (rx_length == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nMensagem de retorno: %f \n",rx_buffer);
	return rx_buffer;
		} 
	}

//Fecha UART
	close(uart0_filestream);
	return 0;
}

}



float tensao_bateria(){

	unsigned char p_tx_buffer []= {0xB2};
	config_uart(); //configuração da uart
	if (uart0_filestream != -1){
	int count = write(uart0_filestream, p_tx_buffer, 1);   // Arquivo, bytes a serem escritos, número de bytes a serem escritos;
	if (count < 0){
	printf("Erro na transmissao - UART TX\n");
		}
	

//RX - Aguarda resposta de retorno 
	
	if (uart0_filestream != -1){
	float rx_buffer;
	int rx_length = read(uart0_filestream, &rx_buffer,1); //Arquivo, buffer de saída, número máximo de caracteres a serem lidos

	if (rx_length < 0){
	perror("Falha na leitura\n");
	return -1;
	}
	else if (rx_length == 0){
	printf("Nenhum dado disponivel\n");
	}
	else{
//Bytes received
	printf("\nMensagem de retorno: %f \n",rx_buffer);
	return rx_buffer;
		} 
	}

//Fecha UART
	close(uart0_filestream);
	return 0;
}

}

//---------------------------------------------------Calculo dos Passos-----------------------------------------------------------------------------


void calculo_passo(){

float circunferencia;

	circunferencia = pi * diametro_engrenagem;			//calcula a circunferencia da engrenagem
	passo = circunferencia/numero_passos;    	                //calcula quantos cm equivale a um passo

}

void calculo_percurso(){
int passos_volta;
 int passos_x_int, passos_x_2_int, passos_x_3_int;
 float passos_x, passos_x_2, passos_x_3, distancia_inicio, distancia_final, tempo_total_um, tempo_total_dois, distancia_pecorrida;
 float distancia_percurso_inteiro;
 float periodo_x_um, periodo_x_dois, periodo_x_tres;	

//caluculo da distancia para inicio da limpeza	
	distancia_inicio = posicao_inicial * placa;
	printf("\n A distancia a ser pecorrida para inicio da limpeza eh: %f cm", distancia_inicio);
	
//calculo dos passos referente a posicao de inicio de limpeza
	passos_x_int = (distancia_inicio/passo)/4;
	//passos_x_int = round(passos_x);				//arredonda o calculo para o valor mais próximo inteiro
	printf("\n A quantidade de passos a pecorrido até o ponto de inicio da limpeza eh: %d", passos_x_int);


//caluculo da distancia para o fim de limpeza	
	distancia_final = posicao_final * placa;
	printf("\n A distancia final a ser pecorrida para termino de limpeza eh: %f", distancia_final);
	
//calculo dos passos referente ao trajeto de limpeza
	distancia_pecorrida = distancia_final - distancia_inicio;
	printf("\n distancia a ser pecorrida a partir do inicio da limpeza é de: %f cm", distancia_pecorrida);
	passos_x_2_int = (distancia_pecorrida/passo)/4;
	//passos_x_2_int = round(passos_x_2);				//arredonda o calculo para o valor mais próximo inteiro
	printf("\n A quantidade de passos a ser pecorrido a partir do incio da limpesa será de: %d passos", passos_x_2_int);


//calculo dos passos referente ao trajeto de volta para a base
	passos_x_3_int = (distancia_final/passo)/4;
	//passos_x_3_int = round(passos_x_3);				//arredonda o calculo para o valor mais próximo inteiro
	printf("\n A quantidade de passos para o trajeto de volta sera de: %d passos \n", passos_x_3_int);


//calculo dos passos de volta
	passos_volta = passos_x_3_int - passos_x_int;



//calculo da distancia do percurso inteiro
//	distancia_percurso_inteiro = 2 * distancia_final;

//calculo do tempo total para o primeiro segmento de reta
//	tempo_total_um = distancia_inicio/velocidade;
//	printf("\n tempo_total_um: %fs", tempo_total_um);

	
//Fazer condição para rotina completa

//Caso não seja rotina completa
//Acionando primeiro movimento
int ret_motor_passo;
int mover, bomba;
int retorno = 0;
int retorno_calha = 0;
int retorno_motor =0;
int retorno_bomba = 0;
int direcao, estado;

	    mover = 1;  
	    aciona_calha(mover);
	    //retorno_calha = aciona_calha(mover);    //manda mensagem via rádio para virar a calha para baixo
/*
	      printf("\nRetorno da calha: %d", retorno_calha); 
	    if(retorno_calha==1){
		printf("Calha foi movida para baixo!");
		//Registrar no arquivo de log
		}
	    if(retorno_calha==0){
		printf("Erro ao mover a calha");
		//Registrar no arquivo de log
		}
		retorno_calha = 0;*/

	    sleep(3);

	    direcao = 1;	
	    passa_codigo();
	    aciona_motor_passo(passos_x_3_int);
	    passa_parametro(direcao);
	    /*retorno_motor = passa_parametro(direcao);
	    if(retorno_motor!=49){
		printf("Ocorreu um erro no primeiro percurso do motor!");
		//Registrar no arquivo de log
		}  
		retorno_motor = 0;*/

	    sleep(3);	    

	    mover = 0;  
	    aciona_calha(mover);
	   /* retorno_calha = aciona_calha(mover);    //manda mensagem via rádio para virar a calha para cima
	   if(retorno_calha!=1){
		printf("Ocorreu um erro ao girar calha para cima!");
		//Registrar no arquivo de log
		}  
		retorno_calha = 0;*/

	    sleep(5);

	    bomba = 1;              //manda mensagem via rádio para acionar a bomba
	    aciona_bomba(bomba);
	    /*retorno_bomba = aciona_bomba(bomba);
	    if(retorno_bomba!=49){
		printf("Ocorreu um erro ao acionar a bomba!");
		//Registrar no arquivo de log
		}  
	    retorno_bomba = 0;*/
            sleep(12);
	    direcao = 0;
 	    passa_codigo();
            aciona_motor_passo(passos_volta);
	    passa_parametro(direcao);
	    /*aciona_motor_passo(passos_volta);
	    retorno_motor = passa_parametro(direcao);
	    if(retorno_motor!=49){
		printf("Ocorreu um erro no terceiro movimento do motor!");
		//Registrar no arquivo de log
		}  
		retorno_motor = 0;*/
	  
	     bomba = 0;              //manda mensagem via rádio para acionar a bomba
	     aciona_bomba(bomba);
	    /*retorno_bomba = aciona_bomba(bomba);
		if(retorno_bomba!=49){
		printf("Ocorreu um erro ao desligar a bomba!");
		//Registrar no arquivo de log
		}  
		retorno_bomba = 0;*/
 		
		sleep(10); 

	    direcao = 0;
 	    passa_codigo();
	    aciona_motor_passo(passos_x_int);
	    passa_parametro(direcao);
	    /*retorno_motor = passa_parametro(direcao);
	    if(retorno_motor!=49){
		printf("Ocorreu um erro no terceiro movimento do motor!");
		//Registrar no arquivo de log
		}  
		retorno_motor = 0; */	   
}

void tratar_informacao(char *buffer){

	int estado;  // variável que liga ou desliga o robo

	memcpy(&estado,   &buffer[0], 4);
	printf("Estado: %d \n", estado);	

	if (estado == 0)  // parada de emergencia, para o robo de qualquer forma e retorna para a base
	{
	//calculo_retorno();		
	}
	if (estado == 1) // Aciona o robo
	{

	memcpy(&posicao_inicial,           &buffer[4], 4);  //passa a informação da posição inicial da placa para a variável posiçao_inicial 
	memcpy(&posicao_final,             &buffer[8], 4);  //passa a informação da posição final da placa para a variável posiçao_final 

	printf("posicao inicial: %d \n", posicao_inicial);
	printf("posicao final: %d \n", posicao_final);
	//verifica_condicoes_iniciais();
	ler_sensores();
	calculo_percurso();
	}
}


//Função que recebe e trata os comando recebidos do cliente
void TrataClienteTCP() {

	char buffer[12];
        int tamanhoRecebido;

        //Receive
        if((tamanhoRecebido = recv(socket_Cliente, &buffer, 12, 0)) < 0) {
            printf("Erro no recv()\n");
        }

	tratar_informacao(buffer);

    }

//---------------------------------------------------Servidor8000/4000 - Comunicação TCP/IP -----------------------------------------------------

void* servidor_8000 () {
        
        struct sockaddr_in servidorAddr, clienteAddr;
        unsigned short porta_servidor;
        unsigned int lenght_cliente;


        //Abrir Socket
        if((socket_Servidor = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
            printf("Falha da abertura do Socket\n");
            exit(1);
        }
        //Preparar Estrutura SockAddr_in
        memset(&servidorAddr, 0, sizeof(servidorAddr));
        servidorAddr.sin_family = AF_INET;
        servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servidorAddr.sin_port = htons(porta);

        //Bind
        if(bind(socket_Servidor, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0) {
            printf("Falha no Bind\n");
            exit(1);
        }

        //Listen
        if(listen(socket_Servidor, 10) < 0) { //coloca o socket em estado de esculta (socket, quantidade de socket que pode acessar o sistema)
            printf("Falha no Listen\n");
            exit(1);
        }

        //Accept
        while(1) {
            lenght_cliente = sizeof(clienteAddr);
            if((socket_Cliente = accept(socket_Servidor, (struct sockaddr *) &clienteAddr, &lenght_cliente)) < 0) {
                printf("Falha no Accept\n");
            }
            TrataClienteTCP();
            close(socket_Cliente);
            }

        close(socket_Servidor);

    }


// Servidor Porta 4000
void* servidor_4000 (void *arg) {
	int socket_Servidor, socket_Cliente;
	struct sockaddr_in servidorAddr, clienteAddr;
	unsigned short porta_servidor;
	unsigned int lenght_cliente;
	

	//Abrir Socket
	if((socket_Servidor = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		printf("Falha da abertura do Socket\n");
		exit(1);
	}
	//Preparar Estrutura SockAddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr));
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servidorAddr.sin_port = htons(4000);
	
	//Bind
	if(bind(socket_Servidor, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0) {
		printf("Falha no Bind\n");
		exit(1);
	}

	//Listen
	if(listen(socket_Servidor, 10) < 0) {
		printf("Falha no Listen\n");
		exit(1);
	}
	
	//Accept
	while(1) {
		lenght_cliente = sizeof(clienteAddr);
		if((socket_Cliente = accept(socket_Servidor, (struct sockaddr *) &clienteAddr, &lenght_cliente)) < 0) {
			printf("Falha no Accept\n");
		} 
		TrataClienteTCP(socket_Cliente);
		close(socket_Cliente);
		}

	close(socket_Servidor);
	
	return NULL;
}

void* verifica_sensores(void *arg) {
	
	ler_sensores();
	sleep(10);
	//Enviar para o cliente os valores atualizados	
	return NULL;
}


//-------------------------------------------Verifica Condições Iniciais do sistema---------------------------------------------------------
/*

//Obs: sensor de chuva deve estar na primeira e na ultima placa
//Gerar o arquivo de log com estas verificações

void verifica_condicoes_iniciais(){
int sensor_1, sensor_2, nivel;
float bateria;

	    mensagem_radio = 0;      
	    sensor_1 = radio_comunica();     //solicita informação do sensor de chuva 1
		
	    if(sensor_1 == 1){
		printf("Está chovendo no senosr 1, a limpeza não sera executada, tente novamente mais tarde!!");
		//Informar para o usuário
		servidor_8000();
	    }
		
	    
	    mensagem_radio = 1;      
	    sensor_2 = radio_comunica();     //solicita informação do sensor de chuva 2

	   if(sensor_2 == 1){
		if(posicao_final <= 3)

		printf("Está chovendo na placa 3 deseja continuar a limpeza??");
		//Informar para o usuário
	    }
	    
	    mensagem_radio = 2;      
	    nivel = radio_comunica();     //solicita informação do sensor de nivel

	     if(nivel == 1){
		printf("Nível de auga no reservatorio esta critico, reabasteça para solicitar a limpeza!!");
		//Informar para o usuário
		servidor_8000();
	    }
 		
	    bateria = tensao_bateria();
	    
	     if(bateria <= 7){
		printf("O sistema nao tem carga na bateria suficuente para realizar a limpeza!!");
		//Informar para o usuário
		servidor_8000();
	    }
	    else{
		printf("A limpeza está sendo inicializada...");
		//informar para o usuário
		}
}

*/
//--------------------------------------------Tratamento de sinal---------------------------------------------------------------------------

void sinal(){

	close(socket_Servidor);
	arquivo(fechar);
	arquivo2(fechar);
	exit(1);	
}

void sinal2(){

	close(socket_Servidor);
	arquivo(fechar);
	arquivo2(fechar);
	exit(1);	
}

//-----------------------------------------------Função principal---------------------------------------------------------------------------

int main () {

	arquivo(inicio);
	arquivo2(inicio);
 
	signal(SIGTSTP, sinal); //caso o usuário encerre o processor usando crt+z
	signal(SIGINT, sinal2); //caso o usuário encerre o processor usando crt+c

	calculo_passo();                //realiza o calculo de quanto equivale o passo dado o valor do diâmetro da engrenagem 
	//limpar_serial();

	pthread_t t0;
	pthread_t t1;
	//pthread_t t2;
	
	int res0, res1, res2;

	res0 = pthread_create(&t0, NULL, servidor_8000, NULL);
	res1 = pthread_create(&t1, NULL, servidor_4000, NULL);
	//res2 = pthread_create(&t2, NULL, verifica_sensores, NULL);
	
	res0 = pthread_join(t0, NULL);
	res1 = pthread_join(t1, NULL);
	//res2 = pthread_join(t2, NULL);

        return 0;
    }

